// File:        tester.h
// Author:      David Barry
// Date:        03/17/2019
// Description: This file will test FindWords for accuracy

#ifndef TESTFINDWORDS_H
#define TESTFINDWORDS_H

#include <set>
#include "FindWords.h"

using namespace std;

class TestFindWords {
public:
	// Default Constructor
	TestFindWords() {}

	// Name:          TestWordSearch
	// Preconditons:  None
	// Postcoditions: Detrimins if this wordSearch is accurate
	bool TestWordSearch();

	// Name:          TestResults
	// Preconditons:  None
	// Postcoditions: Detrimins if FindWord outputs correct word
	bool TestResult();

	// Name:          TestExpetions
	// Preconditons:  None
	// Postcoditions: Tests if expections wordk correctly in FindWords class
	bool TestExceptions();

private:
	// Note all of these values are hardcoded in and must be changed to test each
	// different word search puzzle
	vector<vector<char> > correctWordSearch;
	vector<tuple<int, int, int, int> > correctAnswers;
};

#endif
