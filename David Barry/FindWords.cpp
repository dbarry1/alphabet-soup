// File:        FindWords.cpp
// Author:      David Barry
// Date:        03/17/2019
// Description: This file imports a word search puzzle and finds all the words 
//              in the puzzle.

#include <iostream>
#include <fstream>
#include <stdexcept>
#include "FindWords.h"

using namespace std;

// Name:           WordSearch (Overloaded Constructor)
// Preconditions:  There exists a file with a word search in it 
// Postconditions: Imports wordsearch into a 2D vector, imports words to find
//                 into a string vector
WordSearch::WordSearch(string fileName) {
	ifstream inStr;
	// open file
	inStr.open(fileName);

	// Read in first line
	char size[8]; // max size is 999x999

	// Intilize size
	for (int i = 0; i < 8; i++)
		size[i] = '\0';

	inStr.getline(size, 8);

	// seperate height and width and cast as ints
	int i = 0;
	int temp = 0;
	int height = 0;
	int width = 0;

	// get hieght
	while (size[i] != 'x') {
		// Check size
		if (i >= 3)
			throw out_of_range("the word search is too large");

		// Adjust height
		height *= 10;
		temp = int(size[i]);
		temp -= 48; // ASCII to Bianary conversion
		height += temp;
		i++;
	}
	i++; // remove x 

	// get width
	while (size[i] != '\0') {
		// Check size
		if (i >= 7)
			throw out_of_range("the word search is too large");

		// Adjust height
		width *= 10;
		temp = int(size[i]);
		temp -= 48; // ASCII to Bianary conversion
		width += temp;
		i++;
	}
	// read in word search
	char tempChar;
	vector<char> tempRow;
	for (int j = 0; j < height; j++) {
		tempRow.clear();
		for (int k = 0; k < width; k++) {
			// Read in a row
			inStr >> tempChar;
			tempRow.push_back(tempChar);
		}
		// Insert row into vector
		m_wordSearch.push_back(tempRow);
	}
	// Read in words to find
	char tempWord[80];
	string tempString;

	// Get first input
	inStr.getline(tempWord, 80);

	// while there is data to read in
	while (inStr.eof() == false) {
		tempString = string(tempWord);

		if (tempString.empty() == false) {
			// remove spaces
			for (string::iterator it = tempString.begin(); it != tempString.end(); ++it){
			if (*it == ' ')
				tempString.erase(it);
			}
			// put onto vector
			m_wordsToFind.push_back(tempString);
		}
		tempString.clear();
		inStr.getline(tempWord, 80);
	}
	// close file
	inStr.close();
}
// Name:          getNumWords
// Preconditions: There is a WordSearch object
// Postconditons: Returns the number of words to find
unsigned int WordSearch::getNumWords() {
	return m_wordsToFind.size();
}
// Name:          getWord
// Preconditions: There is a WordSearch object
// Postconditons: Returns the word at the correct index
string WordSearch::getWord(unsigned int index) {
	if (index >= m_wordsToFind.size() || index < 0) {
		throw out_of_range("index is too small or too large");
	}
	else {
		return m_wordsToFind[index];
	}
}
// Name:           FindWord
// Preconditions:  A WordSearch object exists
// PostConditions: Finds word in vector index i and returns the start and stop
//                 indices of the word in a tuple
tuple<int, int, int, int> WordSearch::FindWord(unsigned int index) {
	if (index < 0 or index >= m_wordsToFind.size())
		throw out_of_range("index out of bouds");

	// extract the word to find
	string wordToFind = m_wordsToFind[index];

	// bolean flag
	bool moveNext = true;

	// start and stop indices 
	int rowStart = -1;
	int colStart = -1;
	int rowEnd = -1;
	int colEnd = -1;

	// search for the first letter
	for (unsigned int i = 0; i < m_wordSearch.size(); i++) {
		for (unsigned int j = 0; j < m_wordSearch[i].size(); j++) {
		
			if (wordToFind[0] == m_wordSearch[i][j]) {
				// Search neighbors clockwise
				// Up
				if (i >= 1 && wordToFind[1] == m_wordSearch[i - 1][j]) {
					// Needed varibles
					int k = i - 2;
					int m = j;
					int index = 2;
					
					// If the word has only two letters
					if (index == wordToFind.size()) {
						rowStart = i;
						rowEnd = k + 1;
						colStart = j;
						colEnd = m;
					}
					else {
						while (moveNext == true) {
							// If we are at the boundry
							bool atBoundry = k == 0;
							bool pastBoundry = k < 0;
							bool endOfWord = index == wordToFind.size() - 1;
							if (not pastBoundry && (atBoundry || endOfWord)) {
								moveNext = false;
								if (index == wordToFind.size() - 1 && m_wordSearch[k][m] == wordToFind[index]) {
									rowStart = i;
									rowEnd = k;
									colStart = j;
									colEnd = m;
								}
							}
							// If the word doesn't match
							if (pastBoundry || m_wordSearch[k][m] != wordToFind[index])
								moveNext = false;

							// Update indices
							k--;
							index++;
						}
					}
				}
				// Upper left
				moveNext = true;
				if (i >= 1 && j >= 1 && wordToFind[1] == m_wordSearch[i - 1][j - 1]) {
					// Needed varibles
					int k = i - 2;
					int m = j - 2;
					int index = 2;

					// If the word has only two letters
					if (index == wordToFind.size()) {
						rowStart = i;
						rowEnd = k + 1;
						colStart = j;
						colEnd = m + 1;
					}
					else {
						while (moveNext == true) {
							// If we are at the boundry
							bool atBoundry = k == 0 || m == 0;
							bool pastBoundry = k < 0 || m < 0;
							bool endOfWord = index == wordToFind.size() - 1;
							if (not pastBoundry && (atBoundry || endOfWord)) {
								moveNext = false;
								if (index == wordToFind.size() - 1 && m_wordSearch[k][m] == wordToFind[index]) {
									rowStart = i;
									rowEnd = k;
									colStart = j;
									colEnd = m;
								}
							}
							// If the word doesn't match
							if (pastBoundry || m_wordSearch[k][m] != wordToFind[index])
								moveNext = false;
							// Update indices
							k--;
							m--;
							index++;
						}
					}
				}
				// Left
				moveNext = true;
				if (j >= 1 && wordToFind[1] == m_wordSearch[i][j - 1]) {
					// Needed varibles
					int k = i;
					int m = j - 2;
					int index = 2;

					// If the word has only two letters
					if (index == wordToFind.size()) {
						rowStart = i;
						rowEnd = k;
						colStart = j;
						colEnd = m + 1;
					}
					else {
						while (moveNext == true) {
							// If we are at the boundry
							bool atBoundry = m == 0;
							bool pastBoundry = m < 0;
							bool endOfWord = index == wordToFind.size() - 1;
							if (not pastBoundry && (atBoundry || endOfWord)) {
								moveNext = false;
								if (index == wordToFind.size() - 1 && m_wordSearch[k][m] == wordToFind[index]) {
									rowStart = i;
									rowEnd = k;
									colStart = j;
									colEnd = m;
								}
							}
							// If the word doesn't match
							if (pastBoundry || m_wordSearch[k][m] != wordToFind[index])
								moveNext = false;

							// Update indices
							m--;
							index++;
						}
					}
				}
				// Lower left
				moveNext = true;
				if (i < m_wordSearch.size() - 1 && j >= 1 && wordToFind[1] == m_wordSearch[i + 1][j - 1]) {
					// Needed varibles
					int k = i + 2;
					int m = j - 2;
					int index = 2;

					// If the word has only two letters
					if (index == wordToFind.size()) {
						rowStart = i;
						rowEnd = k - 1;
						colStart = j;
						colEnd = m + 1;
					}
					else {
						while (moveNext == true) {
							// If we are at the boundry
							bool atBoundry = m == 0 || k == m_wordSearch.size() - 1;
							bool pastBoundry = m < 0 || k > m_wordSearch.size() - 1;
							bool endOfWord = index == wordToFind.size() - 1;

							if (not pastBoundry && (endOfWord || atBoundry)) {
								moveNext = false;

								if (index == wordToFind.size() - 1 && m_wordSearch[k][m] == wordToFind[index]) {
									rowStart = i;
									rowEnd = k;
									colStart = j;
									colEnd = m;
								}
							}
							// If the word doesn't match
							if (pastBoundry || m_wordSearch[k][m] != wordToFind[index]) {
								moveNext = false;
							}

							// Update indices
							m--;
							k++;
							index++;
						}
					}
				}
				// Down
				moveNext = true;
				if (i < m_wordSearch.size() - 1 && wordToFind[1] == m_wordSearch[i + 1][j]) {
					// Needed varibles
					int k = i + 2;
					int m = j;
					int index = 2;

					// If the word has only two letters
					if (index == wordToFind.size()) {
						rowStart = i;
						rowEnd = k - 1;
						colStart = j;
						colEnd = m;
					}
					else {
						while (moveNext == true) {
							// If we are at the boundry
							bool atBoundry = k == m_wordSearch.size() - 1;
							bool pastBoundry = k > m_wordSearch.size() - 1;
							bool endOfWord = index == wordToFind.size() - 1;
							if (not pastBoundry && (atBoundry || endOfWord)) {
								moveNext = false;
								if (index == wordToFind.size() - 1 && m_wordSearch[k][m] == wordToFind[index]) {
									rowStart = i;
									rowEnd = k;
									colStart = j;
									colEnd = m;
								}
							}
							// If the word doesn't match
							if (pastBoundry || m_wordSearch[k][m] != wordToFind[index]) {
								moveNext = false;
							}

							// Update indices
							k++;
							index++;
						}
					}
				}
				// Lower Right
				moveNext = true;
				if (i < m_wordSearch.size() - 1 && j < m_wordSearch.size() - 1 && wordToFind[1] == m_wordSearch[i + 1][j + 1]) {
					// Needed varibles
					int k = i + 2;
					int m = j + 2;
					int index = 2;

					// If the word has only two letters
					if (index == wordToFind.size()) {
						rowStart = i;
						rowEnd = k - 1;
						colStart = j;
						colEnd = m - 1;
					}
					else {
						while (moveNext == true) {
							// If we are at the boundry
							bool atBoundry = m == m_wordSearch.size() - 1 || k == m_wordSearch.size() - 1;
							bool pastBoundry = m > m_wordSearch.size() - 1 || k > m_wordSearch.size() - 1;
							bool endOfWord = index == wordToFind.size() - 1;
							if (not pastBoundry && (atBoundry || endOfWord)) {
								moveNext = false;
								if (index == wordToFind.size() - 1 && m_wordSearch[k][m] == wordToFind[index]) {
									rowStart = i;
									rowEnd = k;
									colStart = j;
									colEnd = m;
								}
							}
							// If the word doesn't match
							if (pastBoundry || m_wordSearch[k][m] != wordToFind[index])
								moveNext = false;

							// Update indices
							m++;
							k++;
							index++;
						}
					}
				}
				// Right
				moveNext = true;
				if (j < m_wordSearch.size() - 1 && wordToFind[1] == m_wordSearch[i][j + 1]) {
					// Needed varibles
					int k = i;
					int m = j + 2;
					int index = 2;

					// If the word has only two letters
					if (index == wordToFind.size()) {
						rowStart = i;
						rowEnd = k;
						colStart = j;
						colEnd = m - 1;
					}
					else {
						while (moveNext == true) {
							// If we are at the boundry
							bool atBoundry = m == m_wordSearch.size() - 1;
							bool pastBoundry = m > m_wordSearch.size() - 1;
							bool endOfWord = index == wordToFind.size() - 1;

							if (not pastBoundry && (atBoundry || endOfWord)) {
								moveNext = false;

								if (index == wordToFind.size() - 1 && m_wordSearch[k][m] == wordToFind[index]) {
									rowStart = i;
									rowEnd = k;
									colStart = j;
									colEnd = m;
								}
							}
							// If the word doesn't match
							if (pastBoundry || m_wordSearch[k][m] != wordToFind[index])
								moveNext = false;

							// Update indices
							m++;
							index++;
						}
					}
				}
				// Uper Right
				moveNext = true;
				if (i >= 1 && j < m_wordSearch.size() - 1 && wordToFind[1] == m_wordSearch[i - 1][j + 1]) {
					// Needed varibles
					int k = i - 2;
					int m = j + 2;
					int index = 2;

					// If the word has only two letters
					if (index == wordToFind.size()) {
						rowStart = i;
						rowEnd = k + 1;
						colStart = j;
						colEnd = m - 1;
					}
					else {
						while (moveNext == true) {
							// If we are at the boundry
							bool atBoundry = m == m_wordSearch.size() - 1 || k == 0;
							bool pastBoundry = m > m_wordSearch.size() - 1 || k < 0;
							bool endOfWord = index == wordToFind.size() - 1;

							if (not pastBoundry && (atBoundry || endOfWord)) {
								moveNext = false;

								if (index == wordToFind.size() - 1 && m_wordSearch[k][m] == wordToFind[index]) {
									rowStart = i;
									rowEnd = k;
									colStart = j;
									colEnd = m;
								}
							}
							// If the word doesn't match
							if (pastBoundry || m_wordSearch[k][m] != wordToFind[index])
								moveNext = false;

							// Update indices
							m++;
							k--;
							index++;
						}
					}
				}
			}
		}
	}
	// if one or more of these is -1 the word was not found
	return { rowStart, colStart, rowEnd, colEnd };
}