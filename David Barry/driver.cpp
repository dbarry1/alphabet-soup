// File:        driver.cpp
// Author:      David Barry
// Date:        03/17/2019
// Description: This file will run FindWords.cpp and TestFindWords.cpp

#include "FindWords.h"
#include "TestFindWords.h"
#include <iostream>

using namespace std;

int main() {
	// Find Words
	WordSearch w1("input4.txt");
	for (int i = 0; i < w1.getNumWords(); i++) {
		tuple<int, int, int, int> tempWord = w1.FindWord(i);
		// Note if one or more of these values is -1 the word was
		// not found
		cout << w1.getWord(i) << " " << get<0>(tempWord) << ":" 
			 << get<1>(tempWord) << " " << get<2>(tempWord) << ":" 
			 << get<3>(tempWord) << endl;
	}
	cout << endl;

	// Test if done correctly
	TestFindWords test1;
	bool corrIn = test1.TestWordSearch();
	bool corrRlts = test1.TestResult();
	bool corrExpt = test1.TestExceptions();
	
	bool allCorr = corrExpt && corrIn && corrRlts;
	if (allCorr == true) {
		cout << "All tests passed." << endl << endl;
	}
	system("pause");
	return 0;
}