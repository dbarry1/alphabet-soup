// File:        FindWords.cpp
// Author:      David Barry
// Date:        03/17/2019
// Description: This file imports a word search puzzle and finds all the words 
//              in the puzzle.

#ifndef FINDWORDS_H
#define FINDWORDS_H

#include <vector>
#include <string>
#include <tuple>

using namespace std;

class WordSearch {
	friend class TestFindWords;
public:
	// Name:           WordSearch (Overloaded Constructor)
	// Preconditions:  There exists a file with a word search in it 
	// Postconditions: Imports wordsearch into a 2D vector, imports words to find
	//                 into a string vector
	WordSearch(string fileName);

	// Name:          getNumWords
	// Preconditions: There is a WordSearch object
	// Postconditons: Returns the number of words to find
	unsigned int getNumWords();

	// Name:          getWord
	// Preconditions: There is a WordSearch object
	// Postconditons: Returns the word at the correct index
	string getWord(unsigned int index);

	// Name:           FindWord
	// Preconditions:  A WordSearch object exists
	// PostConditions: Finds word in vector index i and returns the start and stop
	//                 indices of the word in a tuple
	tuple<int,int,int,int> FindWord(unsigned int index);

private:
	vector<vector<char> > m_wordSearch;
	vector<string> m_wordsToFind;
};

#endif

unsigned int getNumWords();
