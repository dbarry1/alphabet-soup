// File:        tester.cpp
// Author:      David Barry
// Date:        03/17/2019
// Description: This file will test FindWords for accuracy

#include "TestFindWords.h"
#include <fstream>
#include <iostream>

using namespace std;

// Name:          TestWordSearch
// Preconditons:  None
// Postcoditions: Detrimins if this wordSearch is accurate
bool TestFindWords::TestWordSearch() {
	bool isCorrect = true;
	cout << "Testing if word search read in correctly..." << endl;

	// Initize Word Search
	WordSearch w1("input3.txt");
	
	// inport correct data
	char correctChar;
	// From...
	ifstream inStr;
	// open file
	inStr.open("testInput3.txt");

	// Check against the correct answer
	for (int i = 0; i < w1.m_wordSearch.size(); i++) {
		for (int j = 0; j < w1.m_wordSearch[i].size(); j++) {
			inStr >> correctChar;
			if (correctChar != w1.m_wordSearch[i][j]) {
				isCorrect = false;
				cout << "Error in m_WordSearch row " << i 
					<< " and column " << j << "." << endl;
			}
		}
	}
	// check if all characters have been read in to the file
	inStr >> correctChar;
	if (inStr.eof() == false) {
		isCorrect = false;
		cout << "Not all letter have been read in." << endl;
	}
	// close file
	inStr.close();

	if (isCorrect == true) {
		cout << "All input tests passed." << endl;
	}
	cout << endl;
	return isCorrect;
}
// Name:          TestResults
// Preconditons:  None
// Postcoditions: Detrimins if FindWord outputs correct word
bool TestFindWords::TestResult() {
	bool isCorrect = true;
	cout << "Testing results of word search..." << endl;
	// Initilize a word search
	WordSearch w1("input3.txt");

	// inport correct data
	char tempInt;
	int correctInt;
	// From...
	ifstream inStr;
	// open file
	inStr.open("testResults3.txt");

	for (int i = 0; i < w1.getNumWords(); i++) {
		tuple<int, int, int, int> tempWord = w1.FindWord(i);
		
		// Test all four ints, First
		// Get Bianary from ASCII
		inStr >> tempInt;
		correctInt = int(tempInt) - 48;
		//cout << "Is end of file: " << inStr.eof() << endl;
		// Test index
		if (get<0>(tempWord) != correctInt) {
			isCorrect = false;
			cout << "Word number " << i << " row start is incorrect."
				<< endl;
		}
		// Second
		// Get Bianary from ASCII
		inStr >> tempInt;
		correctInt = int(tempInt) - 48;

		// test Index
		if (get<1>(tempWord) != correctInt) {
			isCorrect = false;
			cout << "Word number " << i << " row end is incorrect."
				<< endl;
		}
		// Third
		// Get Bianary from ASCII
		inStr >> tempInt;
		correctInt = int(tempInt) - 48;

		// Test index
		if (get<2>(tempWord) != correctInt) {
			isCorrect = false;
			cout << "Word number " << i << " column start is incorrect."
				<< endl;
		}
		// Forth
		// Get Bianary from ASCII
		inStr >> tempInt;
		correctInt = int(tempInt) - 48;

		// Test index
		if (get<3>(tempWord) != correctInt) {
			isCorrect = false;
			cout << "Word number " << i << " column end is incorrect."
				<< endl;
		}
	}

	// Test if all words have been found
	inStr >> correctInt;
	if (inStr.eof() == false) {
		isCorrect = false;
		cout << "Not all words have been found." << endl;
	}
	// close file
	inStr.close();

	if (isCorrect == true) {
		cout << "All result tests passed." << endl;
	}
	cout << endl;
	return isCorrect;
}
// Name:          TestExpetions
// Preconditons:  None
// Postcoditions: Tests if expections wordk correctly in FindWords class
bool TestFindWords::TestExceptions() {
	// test if exceptions are caught correctly
	cout << "Testing exception handling..." << endl;

	// In constuctor
	bool is1Caught = false;
	try {
		// Bad input (too many rows)
		WordSearch wE1("inputError1.txt");
	}
	catch(out_of_range){
		is1Caught = true;
	}
	bool is2Caught = false;
	try {
		//Bad input 2 (too many columns)
		WordSearch wE2("inputError1.txt");
	}
	catch(out_of_range){
		is2Caught = true;
	}
	// In getWord
	bool is3Caught = false;
	try {
		WordSearch w1("input3.txt");
		// Index too high
		w1.getWord(10);
	}
	catch(out_of_range) {
		is3Caught = true;
	}
	bool is4Caught = false;
	try {
		WordSearch w2("input3.txt");
		// Index too low
		w2.getWord(-1);
	}
	catch (out_of_range) {
		is4Caught = true;
	}
	// In FIndWord
	bool is5Caught = false;
	try {
		WordSearch w3("input3.txt");
		// Index too high
		w3.FindWord(10);
	}
	catch (out_of_range) {
		is5Caught = true;
	}
	bool is6Caught = false;
	try {
		WordSearch w3("input3.txt");
		// Index too low
		w3.FindWord(-1);
	}
	catch (out_of_range) {
		is6Caught = true;
	}
	bool isCorrect = is1Caught && is2Caught && is3Caught && 
		is4Caught && is5Caught && is6Caught;

	if (isCorrect == true) {
		cout << "All exception tested passed." << endl;
	}
	else {
		cout << "One or more tested failed." << endl;
	}
	cout << endl;
	return isCorrect;
}
